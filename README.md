# test-plantuml
```plantuml
title OSEF-POSIX classes

package <<Mutex>> {
    class Mutex
}

package <<Time>> {
    class TimeOut
}

package <<File>> {
    class File
    class AppendFile
    class OverwriteFile
    class ReadFile
}

AppendFile -up-|> File
OverwriteFile -up-|> File
ReadFile -up-|> File
package <<Random>> {
    class Randomizer
}

package <<MsgQ>> {
    class MsgQDirectory
    class MsgQReceiver
    class MsgQReceiver_t
    class MsgQSender
    class MsgQSender_t
}

MsgQDirectory .up. Randomizer
MsgQReceiver o- MsgQDirectory
MsgQReceiver_t -up-|> MsgQReceiver
MsgQSender_t -up-|> MsgQSender

package <<Thread>> {
    class ThreadSpawner
}

Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
```
